import com.fasterxml.jackson.core.JsonProcessingException;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import entity.Tuple;
import kafka.KafkaConnectorProducer;
import org.apache.kafka.common.protocol.types.Field;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Main {

	private static final String KAFKA_URL = "kafka.cini-project.cloud:9092";
	//private static final String KAFKA_URL = "localhost:9092";
	private static final String KAFKA_TOPIC = "comments";
	private static final String FILE_PATH = "/Users/emanuele/Università/magistrale/quinto anno/SABD/Progetto2/Comments_jan-apr2018.csv";
	//private static final int maxTime = 7200;
	private static final int maxTime = 3600;
	private static final double startTime = 1514767661.0;
	private static final double endTime = 1524085781.0;
	private static final double delta = endTime - startTime;

	public static void main(String[] args) throws IOException, InterruptedException {
		KafkaConnectorProducer<String, String> producer = new KafkaConnectorProducer<String, String>(KAFKA_URL);
		realSender(producer);
	}

	private static void realSender(KafkaConnectorProducer<String, String> producer) throws FileNotFoundException, InterruptedException, JsonProcessingException {
		CsvToBean<Tuple> csvToBean = new CsvToBeanBuilder<Tuple>(new FileReader(FILE_PATH))
				.withType(Tuple.class).build();
		Iterator<Tuple> iterator = csvToBean.iterator();
		Tuple tuple = null;
		Long lastTime = 0L, time = null, sleepTime = null, tupleTime = null;
		String json;
		int day = 0;
		int i = 0;
		LocalDateTime localDateTime;
		LocalDateTime old = null;
		while (iterator.hasNext()) {
			tuple = iterator.next();
			tupleTime = Long.valueOf(tuple.getCreateDate());
			time = (long) ((((tupleTime - startTime) / delta)) * maxTime);
			sleepTime = (time - lastTime) * 1000;
			Thread.sleep(sleepTime);
			lastTime = time;

			json = tuple.toJson();
			localDateTime = Instant.ofEpochMilli(tupleTime * 1000).atZone(ZoneId.of("UTC")).toLocalDateTime();
			if(day < localDateTime.getDayOfYear()) {
				day = localDateTime.getDayOfYear();
				System.out.println(localDateTime);
			}
			producer.send(KAFKA_TOPIC, json);

		}

	}

}
